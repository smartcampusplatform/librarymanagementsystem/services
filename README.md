# Smartcampus Library Management System

This service system is a library management system for Smartcampus platform.

---

* [Language](#language)
* [List of API services](#list-of-api-services)
  - [Search Book](#search-book)
  - [Add New Book](#add-new-book)
  - [Get Book Detail By ISBN](#get-book-detail-by-isbn)
  - [Get Book Recommendation Suggestion](#get-book-recommendation-suggestion)
* [Built With](#built-with)
* [Authors](#authors)
* [License](#license)
* [Acknowledgments](#acknowledgments)

---

## Language

This service system is created by using **nodeJS**

## List of API Services

### **Search Book**

---

API for searching book from database.

* **URL**

  ```
  /librarymanagementsystem/book/api/search?q=YOUR_KEYWORD
  ```

* **Method:**

  `GET`

* **URL Query**

  URL query should include keyword to find a book (author/title/category).


* **Success Response:**

  * **Code:** 200 <br/>
**Content:**
``[
{
category: "Buku",
image: "https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png",
publisher: "O'Reilly Media",
published: "2015-12-27T00:00:00.000Z",
goodCondition: 4,
badCondition: 0,
stock: 4,
total: 4,
location: "3-12A",
_id: "5ccd4e99803f7afed94a4785",
isbn: "9781491904244",
title: "You Don't Know JS",
author: "Kyle Simpson",
description: "No matter how much experience you have with JavaScript, odds are you don’t fully understand the language. As part of the "You Don’t Know JS" series, this compact guide focuses on new features available in ECMAScript 6 (ES6), the latest version of the standard upon which JavaScript is built.",
badCondtion: 0
}
]``

* **Sample Call:**

  ```
  curl -i /librarymanagementsystem/book/api/search?q=js
  ```

### **Add New Book**

---

API for adding new book to library collection.

* **URL**

  ```
  /librarymanagementsystem/book/api
  ```

* **Method:**

  `POST`

* **Data Params**

  Body request should included as mentioned below with the content type of application/json.

  **Required:**

```
{
  "title":[varchar],
  "isbn":[varchar],
  "description":[varchar],
  "category":[varchar],
  "author":[varchar]
}
```


* **Success Response:**

  * **Code:** 200 <br />

* **Sample Call:**

  ```
  $.ajax({
    url: "/identitymanagement/api/logout",
    type: "POST",
    data: bookData,
    contentType: "application/json",
    success: function(data){
      // function if success
    },
    error: function (err) {
      // function if error
    }
  });
  ```

### **Get Book Detail By ISBN**

---

API for getting book info given a certain isbn.

* **URL**

  ```
  /librarymanagementsystem/book/api/:isbn
  ```

* **Method:**

  `GET`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `
{
  category: "Buku",
  image: "https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png",
  publisher: "O'Reilly Media",
  published: "2015-12-27T00:00:00.000Z",
  goodCondition: 4,
  badCondition: 0,
  stock: 4,
  total: 4,
  location: "3-12A",
  _id: "5ccd4e99803f7afed94a4785",
  isbn: "9781491904244",
  title: "You Don't Know JS",
  author: "Kyle Simpson",
  description: "No matter how much experience you have with JavaScript, odds are you don’t fully understand the language. As part of the "You Don’t Know JS" series, this compact guide focuses on new features available in ECMAScript 6 (ES6), the latest version of the standard upon which JavaScript is built.",
  badCondtion: 0
}`

* **Sample Call:**

  ```
  curl -i /librarymanagementsystem/book/api/9781491904244
  ```
### **Get Book Recommendation Suggestion**

---

API for getting book recommendation suggestion from other sources.

* **URL**

  ```
  /librarymanagementsystem/recommendation/api/suggestion?q=YOUR_KEYWORD
  ```

* **Method:**

  `GET`

* **URL Query**

  URL query should include keyword to find a book (author/title).


* **Success Response:**

  * **Code:** 200 <br/>
  **Content:**
``[
{
title: "Harry Potter and the Sorcerer's Stone (Harry Potter, #1)",
author: "J.K. Rowling",
image: "https://images.gr-assets.com/books/1474154022s/3.jpg",
published_year: 1997
}
]``

* **Sample Call:**

  ```
  curl -i /librarymanagementsystem/recommendation/api/suggest?q=harry%20potter

## Built With

* [Express](https://expressjs.com/) - The web framework used

## Authors

* **Alessandro Aria Wibowo (18216002)** - [theRealSandro](http://178.128.104.74:9000/prajadimas)

## License

This project is licensed under the MIT License

## Acknowledgments
*MR
